package nu.peg.bomb.modules.button;

/**
 * Joel Messerli am 11.10.2015
 */
public enum ButtonText {
    ABORT, DETONATE, HOLD;

    public static ButtonText getByName(String name) {
        for (ButtonText text : ButtonText.values())
            if (text.name().toLowerCase().equals(name.toLowerCase()))
                return text;

        return null;
    }
}
