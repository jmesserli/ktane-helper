package nu.peg.bomb.modules.button;

/**
 * Joel Messerli am 11.10.2015
 */
public enum StripColor {
    BLUE, WHITE, YELLOW, OTHER;

    public static StripColor getByName(String name) {
        for (StripColor color : StripColor.values())
            if (color.name().toLowerCase().equals(name.toLowerCase()))
                return color;

        return OTHER;
    }
}
