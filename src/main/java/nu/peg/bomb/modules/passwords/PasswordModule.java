package nu.peg.bomb.modules.passwords;

import nu.peg.bomb.modules.Module;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * Joel Messerli am 11.10.2015
 */
public class PasswordModule implements Module<PasswordInput, String> {

    List<String> passwords = Arrays.asList("about", "after", "again", "below", "could", "every",
            "first", "found", "great", "house", "large", "learn", "never", "other", "place",
            "plant", "point", "right", "small", "sound", "spell", "still", "study", "their",
            "there", "these", "thing", "think", "three", "water", "where", "which", "world",
            "would", "write");

    @Override
    public String getSolution(PasswordInput passwordInput) {

        ArrayList<String> matches = new ArrayList<>();
        HashSet<String> dist1match = new HashSet<>();
        HashSet<String> dist2match = new HashSet<>();
        HashSet<String> dist3match = new HashSet<>();

        System.out.printf("Bruteforcing... (%d Possibilities)%n", passwordInput.getFirstChars().size() * passwordInput.getSecondChars().size() * passwordInput.getThirdChars().size() * passwordInput.getFourthChars().size() * passwordInput.getFifthChars().size());

        for (char first : passwordInput.getFirstChars()) {
            for (char second : passwordInput.getSecondChars()) {
                for (char third : passwordInput.getThirdChars()) {
                    for (char fourth : passwordInput.getFourthChars()) {
                        for (char fifth : passwordInput.getFifthChars()) {
                            String currentPassword = String.format("%s%s%s%s%s", first, second, third, fourth, fifth);
                            for (String password : passwords) {
                                if (password.equals(currentPassword)) {
                                    matches.add(currentPassword);
                                    continue;
                                }

                                int hamming = hamming(currentPassword, password);
                                switch (hamming) {
                                    case 1:
                                        dist1match.add(password);
                                        break;
                                    case 2:
                                        dist2match.add(password);
                                        break;
                                    case 3:
                                        dist3match.add(password);
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }

        dist2match.removeAll(dist1match);

        dist3match.removeAll(dist1match);
        dist3match.removeAll(dist2match);

        return String.format("Matches: %s%nHumming_1 Matches: %s%nHumming_2 Matches: %s%nHumming_3 Matches: %s", matches, dist1match, dist2match, dist3match);
    }

    public int hamming(String one, String two) {
        if (one.length() != two.length())
            return -1;

        int dist = 0;
        for (int i = 0; i < one.length(); i++)
            if (one.charAt(i) != two.charAt(i))
                dist++;

        return dist;
    }

    @Override
    public String getModuleName() {
        return "Passwords";
    }

    @Override
    public int getVersion() {
        return 1;
    }
}
