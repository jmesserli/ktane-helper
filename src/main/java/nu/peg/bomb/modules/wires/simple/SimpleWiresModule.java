package nu.peg.bomb.modules.wires.simple;

import nu.peg.bomb.modules.Module;
import nu.peg.bomb.modules.wires.Wire;
import nu.peg.bomb.modules.wires.WireColor;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;

/**
 * Joel Messerli am 10.10.2015
 */
public class SimpleWiresModule implements Module<String, String> {
    @Override
    public String getSolution(String wires) {
        // Parsing
        if (wires.length() < 3 || wires.length() > 6)
            throw new IllegalArgumentException(MessageFormat.format("Illegal amount of wires: {0}", wires.length()));

        if (wires.matches(".*[^rwbyB].*"))
            throw new IllegalArgumentException(String.format("Contains unknown wire color: %s", wires));

        ArrayList<Wire> wireList = new ArrayList<>();
        for (char c : wires.toCharArray())
            wireList.add(new Wire(c));

        switch (wireList.size()) {
            case 3:
                if (!hasColor(wireList, WireColor.RED))
                    return "Cut the second wire";

                if (wireList.get(wireList.size() - 1).getColor() == WireColor.WHITE)
                    return "Cut the last wire";

                if (colorCount(wireList, WireColor.BLUE) > 1)
                    return "Cut the last blue wire";

                return "Cut the last wire";
            case 4:
                if (colorCount(wireList, WireColor.RED) > 1 && serialLastNumberOdd())
                    return "Cut the last red wire";

                if (wireList.get(wireList.size() - 1).getColor() == WireColor.YELLOW && !hasColor(wireList, WireColor.RED))
                    return "Cut the first wire";

                if (colorCount(wireList, WireColor.BLUE) == 1)
                    return "Cut the first wire";

                if (colorCount(wireList, WireColor.YELLOW) > 1)
                    return "Cut the last wire";

                return "Cut the second wire";
            case 5:
                if (wireList.get(wireList.size() - 1).getColor() == WireColor.BLACK && serialLastNumberOdd())
                    return "Cut the first wire";

                if (colorCount(wireList, WireColor.RED) == 1 && colorCount(wireList, WireColor.YELLOW) > 1)
                    return "Cut the first wire";

                if (!hasColor(wireList, WireColor.BLACK))
                    return "Cut the second wire";

                return "Cut the first wire";
            case 6:
                if (!hasColor(wireList, WireColor.YELLOW) && serialLastNumberOdd())
                    return "Cut the third wire";

                if (colorCount(wireList, WireColor.YELLOW) == 1 && colorCount(wireList, WireColor.WHITE) > 1)
                    return "Cut the fourth wire";

                if (!hasColor(wireList, WireColor.RED))
                    return "Cut the last wire";

                return "Cut the fourth wire";
        }

        return null;
    }

    private static boolean serialLastNumberOdd() {
        System.out.print("Enter the last digit of the serial number: ");
        int digit = new Scanner(System.in).nextInt();
        return digit % 2 == 1;
    }

    private static int colorCount(Collection<Wire> wires, WireColor color) {
        return (int) wires.stream().filter(wire -> wire.getColor() == color).count();
    }

    private static boolean hasColor(Collection<Wire> wires, WireColor color) {
        for (Wire wire : wires)
            if (wire.getColor() == color)
                return true;

        return false;
    }

    @Override
    public String getModuleName() {
        return "Simple Wires";
    }

    @Override
    public int getVersion() {
        return 1;
    }
}
