package nu.peg.bomb.modules.button;

/**
 * Joel Messerli am 11.10.2015
 */
public class ButtonInput {
    private ButtonColor color;
    private ButtonText text;

    public ButtonInput(ButtonColor color, ButtonText text) {
        this.color = color;
        this.text = text;
    }

    public ButtonColor getColor() {
        return color;
    }

    public ButtonText getText() {
        return text;
    }
}
