package nu.peg.bomb.modules.button;

import nu.peg.bomb.modules.Module;
import java.util.Scanner;

/**
 * Joel Messerli am 11.10.2015
 */
public class ButtonModule implements Module<ButtonInput, String> {

    @Override
    public String getSolution(ButtonInput input) {
        if (input.getColor() == ButtonColor.BLUE && input.getText() == ButtonText.ABORT)
            return releasingTheButton();

        int batteries = getBatteries();
        if (batteries > 1 && input.getText() == ButtonText.DETONATE)
            return "Press and release";

        if (input.getColor() == ButtonColor.WHITE && hasLit("CAR"))
            return releasingTheButton();

        if (batteries > 2 && hasLit("FRK"))
            return "Press and release";

        if (input.getColor() == ButtonColor.YELLOW)
            return releasingTheButton();

        if (input.getColor() == ButtonColor.RED && input.getText() == ButtonText.HOLD)
            return "Press and release";

        return releasingTheButton();
    }

    private static boolean hasLit(String seq) {
        System.out.printf("Is there a lit '%s'? (y/yes/j): ", seq);
        String input = new Scanner(System.in).nextLine();
        return input.matches("(y(es)?|j(a)?)");
    }

    private static int getBatteries() {
        System.out.print("How many batteries: ");
        return new Scanner(System.in).nextInt();
    }

    private static String releasingTheButton() {
        System.out.print("Hold button, enter color: ");
        StripColor color = StripColor.getByName(new Scanner(System.in).nextLine());

        if (color == StripColor.BLUE)
            return "Release when timer has 4";
        if (color == StripColor.YELLOW)
            return "Release when timer has 5";

        return "Release when timer has 1";
    }

    @Override
    public String getModuleName() {
        return "Button";
    }

    @Override
    public int getVersion() {
        return 1;
    }
}
