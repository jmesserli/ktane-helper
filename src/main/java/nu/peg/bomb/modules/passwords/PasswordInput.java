package nu.peg.bomb.modules.passwords;

import java.util.ArrayList;

/**
 * Joel Messerli am 11.10.2015
 */
public class PasswordInput {

    ArrayList<Character> firstChars;
    ArrayList<Character> secondChars;
    ArrayList<Character> thirdChars;
    ArrayList<Character> fourthChars;
    ArrayList<Character> fifthChars;

    public PasswordInput(String first, String second, String third, String fourth, String fifth) {
        firstChars = distinctArray(first.toCharArray());
        secondChars = distinctArray(second.toCharArray());
        thirdChars = distinctArray(third.toCharArray());
        fourthChars = distinctArray(fourth.toCharArray());
        fifthChars = distinctArray(fifth.toCharArray());
    }

    private ArrayList<Character> distinctArray(char[] charray) {
        ArrayList<Character> characters = new ArrayList<>();
        for (char c : charray) if (!characters.contains(c)) characters.add(c);
        return characters;
    }

    public ArrayList<Character> getFirstChars() {
        return firstChars;
    }

    public ArrayList<Character> getSecondChars() {
        return secondChars;
    }

    public ArrayList<Character> getThirdChars() {
        return thirdChars;
    }

    public ArrayList<Character> getFourthChars() {
        return fourthChars;
    }

    public ArrayList<Character> getFifthChars() {
        return fifthChars;
    }
}
