package nu.peg.bomb.modules.wires.complicated;

import nu.peg.bomb.modules.Module;

import java.util.HashMap;

/**
 * Created by joel on 04.06.2016.
 */
public class ComplicatedWiresModule implements Module<String, String> {

    // FLAGS
    private static final int RED = 1;
    private static final int BLUE = 2;
    private static final int STAR = 4;
    private static final int LED = 8;

    private static HashMap<Character, Integer> characterFlagMap = new HashMap<>();
    private static HashMap<Character, String> solutionMap = new HashMap<>();
    private static HashMap<Integer, Character> flagMap = new HashMap<>();

    // Init charFlagMap
    static {
        characterFlagMap.put('r', RED);
        characterFlagMap.put('b', BLUE);
        characterFlagMap.put('s', STAR);
        characterFlagMap.put('l', LED);
    }

    // Init Solutions
    static {
        solutionMap.put('C', "Cut the wire");
        solutionMap.put('D', "Don't cut the wire");
        solutionMap.put('S', "Cut the wire if the last digit of the serial number is even");
        solutionMap.put('P', "Cut the wire if parallel port");
        solutionMap.put('B', "Cut the wire if batteries > 2");
    }

    // Init FlagMap
    static {
        flagMap.put(0, 'C');
        flagMap.put(RED, 'S');
        flagMap.put(BLUE, 'S');
        flagMap.put(RED | BLUE, 'S');
        flagMap.put(STAR, 'C');
        flagMap.put(RED | STAR, 'C');
        flagMap.put(BLUE | STAR, 'D');
        flagMap.put(RED | BLUE | STAR, 'P');
        flagMap.put(LED, 'D');
        flagMap.put(RED | LED, 'B');
        flagMap.put(BLUE | LED, 'P');
        flagMap.put(RED | BLUE | LED, 'S');
        flagMap.put(STAR | LED, 'B');
        flagMap.put(RED | STAR | LED, 'B');
        flagMap.put(BLUE | STAR | LED, 'P');
        flagMap.put(RED | BLUE | STAR | LED, 'D');
    }

    @Override
    public String getSolution(String s) {
        String[] wires = s.split(",");
        StringBuilder solution = new StringBuilder();

        for (int i = 0; i < wires.length; i++) {
            String wire = wires[i];

            int val = 0;
            for (char c : wire.toCharArray()) val |= characterFlagMap.getOrDefault(c, 0);

            //noinspection SuspiciousMethodCalls
            solution.append(solutionMap.get(flagMap.get(val))).append("\n");
        }

        return solution.toString();
    }

    @Override
    public String getModuleName() {
        return "Complicated Wires";
    }

    @Override
    public int getVersion() {
        return 1;
    }
}
