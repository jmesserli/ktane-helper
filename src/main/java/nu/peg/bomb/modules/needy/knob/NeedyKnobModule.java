package nu.peg.bomb.modules.needy.knob;

import nu.peg.bomb.modules.Module;

import java.util.HashMap;

public class NeedyKnobModule implements Module<String, String> {

    private static HashMap<String, String> solutionMap = new HashMap<>();

    static {
        solutionMap.put("001011111101", "Up");
        solutionMap.put("101010011011", "Up");
        solutionMap.put("011001111101", "Down");
        solutionMap.put("101010010001", "Down");
        solutionMap.put("000010100111", "Left");
        solutionMap.put("000010000110", "Left");
        solutionMap.put("101111111010", "Right");
        solutionMap.put("101100111010", "Right");
    }

    @Override
    public String getSolution(String s) {
        return String.format("Put the knob in the %s position.", solutionMap.get(s));
    }

    @Override
    public String getModuleName() {
        return "Needy Knob";
    }

    @Override
    public int getVersion() {
        return 1;
    }
}
