package nu.peg.bomb;

import nu.peg.bomb.modules.Module;
import nu.peg.bomb.modules.button.ButtonColor;
import nu.peg.bomb.modules.button.ButtonInput;
import nu.peg.bomb.modules.button.ButtonModule;
import nu.peg.bomb.modules.button.ButtonText;
import nu.peg.bomb.modules.memory.MemoryModule;
import nu.peg.bomb.modules.needy.knob.NeedyKnobModule;
import nu.peg.bomb.modules.passwords.PasswordInput;
import nu.peg.bomb.modules.passwords.PasswordModule;
import nu.peg.bomb.modules.wires.complicated.ComplicatedWiresModule;
import nu.peg.bomb.modules.wires.simple.SimpleWiresModule;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class App {
    private static Scanner inScanner = new Scanner(System.in);

    public static void main(String[] args) {
        // MODULES
        SimpleWiresModule simpleWires = new SimpleWiresModule();
        ComplicatedWiresModule complicatedWires = new ComplicatedWiresModule();
        ButtonModule buttonModule = new ButtonModule();
        PasswordModule passwordModule = new PasswordModule();
        MemoryModule memoryModule = new MemoryModule();
        NeedyKnobModule knobModule = new NeedyKnobModule();

        List<Module> modules = Arrays.asList(simpleWires, complicatedWires, buttonModule, passwordModule, memoryModule, knobModule);

        while (true) {
            for (int i = 0; i < modules.size(); i++)
                System.out.printf("%d: %s%n", i + 1, modules.get(i).getModuleName());

            System.out.println();
            int moduleIdx = Integer.parseInt(prompt("Which module? ")) - 1;

            if (moduleIdx > modules.size() - 1 || moduleIdx < 0) {
                System.out.println("Illegal module index");
                continue;
            }

            Module selected = modules.get(moduleIdx);

            System.out.println();

            if (selected instanceof SimpleWiresModule) {
                System.out.println(simpleWires.getSolution(prompt("Enter the wires: ")));
            } else if (selected instanceof ComplicatedWiresModule) {
                System.out.println(complicatedWires.getSolution(prompt("Enter the wires: ")));
            } else if (selected instanceof ButtonModule) {
                ButtonColor color = ButtonColor.getByName(prompt("Button color: "));
                ButtonText text = ButtonText.getByName(prompt("Button text: "));
                ButtonInput input = new ButtonInput(color, text);

                System.out.println(buttonModule.getSolution(input));
            } else if (selected instanceof PasswordModule) {
                String first = prompt("Enter the first characters: ");
                String second = prompt("Enter the second characters: ");
                String third = prompt("Enter the third characters: ");
                String fourth = prompt("Enter the fourth characters: ");
                String fifth = prompt("Enter the fifth characters: ");

                PasswordInput input = new PasswordInput(first, second, third, fourth, fifth);
                System.out.println(passwordModule.getSolution(input));
            } else if (selected instanceof NeedyKnobModule) {
                System.out.println(knobModule.getSolution(prompt("Enter the lights 1/0: ")));
            } else {
                //noinspection unchecked
                selected.getSolution(null);
            }

            System.out.println("\n====================================================");
        }
    }

    private static String prompt(String prompt) {
        System.out.print(prompt);
        return inScanner.nextLine();
    }
}
