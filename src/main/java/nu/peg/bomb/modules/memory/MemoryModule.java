package nu.peg.bomb.modules.memory;

import nu.peg.bomb.modules.Module;

import java.util.ArrayList;
import java.util.Scanner;

import static nu.peg.bomb.modules.memory.MemoryPairEnum.LABEL;
import static nu.peg.bomb.modules.memory.MemoryPairEnum.POSITION;

public class MemoryModule implements Module<Void, Void> {

    private ArrayList<MemoryPair> pairs;
    private int tempDisplay, tempPosition, tempLabel;

    private int promptInt(String prompt) {
        System.out.print(prompt);
        return new Scanner(System.in).nextInt();
    }

    private void doPos(int pos) {
        tempLabel = 0;
        tempPosition = pos;

        System.out.println("Press position " + pos);
    }

    private void doLabel(int label) {
        tempLabel = label;
        tempPosition = 0;

        System.out.println("Press label " + label);
    }

    private void getMissingAndSave() {
        if (tempLabel != 0)
            tempPosition = promptInt("What position was that? ");
        else tempLabel = promptInt("What label was that? ");

        MemoryPair pair = new MemoryPair(tempPosition, tempLabel);
        pairs.add(pair);
        System.out.println();
    }

    private void getDisplay() {
        tempDisplay = promptInt("Number in display: ");
    }

    private int getOld(int stage, MemoryPairEnum pairEnum) {
        MemoryPair pair = pairs.get(stage - 1);

        if (pairEnum == LABEL)
            return pair.label;
        else return pair.position;
    }

    @Override
    public Void getSolution(Void ignored) {
        pairs = new ArrayList<>();

        // STAGE 1
        getDisplay();

        if (tempDisplay == 1) doPos(2);
        else if (tempDisplay == 2) doPos(2);
        else if (tempDisplay == 3) doPos(3);
        else doPos(4);

        getMissingAndSave();

        // STAGE 2
        getDisplay();

        if (tempDisplay == 1) doLabel(4);
        else if (tempDisplay == 2) doPos(getOld(1, POSITION));
        else if (tempDisplay == 3) doPos(1);
        else doPos(getOld(1, POSITION));

        getMissingAndSave();

        // STAGE 3
        getDisplay();

        if (tempDisplay == 1) doLabel(getOld(2, LABEL));
        else if (tempDisplay == 2) doLabel(getOld(1, LABEL));
        else if (tempDisplay == 3) doPos(1);
        else doLabel(4);

        getMissingAndSave();

        // STAGE 4
        getDisplay();

        if (tempDisplay == 1) doPos(getOld(1, POSITION));
        else if (tempDisplay == 2) doPos(1);
        else if (tempDisplay == 3) doPos(getOld(2, POSITION));
        else doPos(getOld(2, POSITION));

        getMissingAndSave();

        // STAGE 5
        getDisplay();

        if (tempDisplay == 1) doLabel(getOld(1, LABEL));
        else if (tempDisplay == 2) doLabel(getOld(2, LABEL));
        else if (tempDisplay == 3) doLabel(getOld(4, LABEL));
        else doLabel(getOld(3, LABEL));


        return null;
    }

    @Override
    public String getModuleName() {
        return "Memory";
    }

    @Override
    public int getVersion() {
        return 1;
    }
}
