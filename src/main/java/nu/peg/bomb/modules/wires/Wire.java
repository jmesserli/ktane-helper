package nu.peg.bomb.modules.wires;

public class Wire {
    private WireColor color;

    public Wire(char representation) {
        this(WireColor.getColor(representation));
    }

    public Wire(WireColor color) {
        if (color == null)
            throw new IllegalArgumentException("WireColor cannot be null");
        this.color = color;
    }

    public WireColor getColor() {
        return color;
    }
}
