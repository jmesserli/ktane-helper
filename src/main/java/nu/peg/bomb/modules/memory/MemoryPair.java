package nu.peg.bomb.modules.memory;

/**
 * Created by joel on 04.06.2016.
 */
public class MemoryPair {
    public int position;
    public int label;

    public MemoryPair(int position, int label) {
        this.position = position;
        this.label = label;
    }
}
