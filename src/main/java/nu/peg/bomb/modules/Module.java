package nu.peg.bomb.modules;

/**
 * Joel Messerli am 10.10.2015
 */
public interface Module<Input, Solution> {
    public Solution getSolution(Input input);

    public String getModuleName();

    public int getVersion();
}
