package nu.peg.bomb.modules.wires;

/**
 * Joel Messerli am 10.10.2015
 */
public enum WireColor {
    RED('r'), WHITE('w'), BLUE('b'), YELLOW('y'), BLACK('B');

    private char representation;

    WireColor(char representation) {
        this.representation = representation;
    }

    public static WireColor getColor(char representation) {
        for (WireColor color : WireColor.values())
            if (color.representation == representation)
                return color;

        return null;
    }
}
