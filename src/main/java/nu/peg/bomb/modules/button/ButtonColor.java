package nu.peg.bomb.modules.button;

/**
 * Joel Messerli am 11.10.2015
 */
public enum ButtonColor {
    BLUE, WHITE, YELLOW, RED;

    public static ButtonColor getByName(String name) {
        for (ButtonColor color : ButtonColor.values())
            if (color.name().toLowerCase().equals(name.toLowerCase()))
                return color;

        return null;
    }
}
